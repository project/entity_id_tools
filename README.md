# Entity ID Tools

This Drupal module provides some administrative tools to work with content
entity IDs.

## Requirements

Just a Drupal site with the [Drush](https://www.drush.org/) utility installed.

## Installation

Install the Entity ID Tools module as you would normally install a contributed
Drupal module. Visit <https://www.drupal.org/node/1897420> for further
information.

## Features

The initial release provides two drush commands:

* entity_id_tools:next_id
  Establishes the ID for the next instance of a given entity type.
* entity_id_tools:next_revision_id
  Establishes the revsion ID for the next instance of a given entity type.

Check the command inline help for more information.

## Limitations

The way entity IDs are managed varies on each database. The module currently
supports MySQL compatible databases only.

## Maintainers

Current maintainers:

* Manuel Adan - <https://www.drupal.org/u/manueladan>
