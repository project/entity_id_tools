<?php

namespace Drupal\entity_id_tools\Commands;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drush\Commands\DrushCommands;

/**
 * A Drush commandfile for entity ID tools.
 */
class EntityIdToolsCommands extends DrushCommands {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Constructs a EntityIdToolsCommands object.
   *
   * @param \Drupal\entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, Connection $database) {
    $this->entityTypeManager = $entity_type_manager;
    $this->database = $database;
  }

  /**
   * Establishes the ID for the next instance of a given entity type.
   *
   * @param string $entity_type_id
   *   The entity type machine name.
   * @param int $next_id
   *   The ID of the next content instance.
   * @param array $options
   *   Additional options for the command.
   *
   * @usage entity_id_tools:next_id node 100
   *   Sets 100 as the next node ID.
   *
   * @command entity_id_tools:next_id
   * @option relaxed
   *   Do not exit with error if new ID is already assigned.
   */
  public function nextId(string $entity_type_id, int $next_id, array $options = [
    'relaxed' => FALSE,
  ]) {
    if ($next_id < 1) {
      // We cannot set entity IDs under 1.
      throw new \InvalidArgumentException(dt('The entity ID must be a positive integer.'));
    }

    $entity_type = $this->getEntityDefinition($entity_type_id);
    /** @var \Drupal\Core\Entity\ContentEntityStorageInterface $storage */
    $storage = $this->entityTypeManager->getStorage($entity_type_id);
    $id_key = $entity_type->getKey('id');
    $last_id_result = $storage->getQuery()
      ->accessCheck(FALSE)
      ->sort($id_key, 'DESC')
      ->range(0, 1)
      ->execute();
    if (!empty($last_id_result) && $next_id <= ($last_id = reset($last_id_result))) {
      // Existing content found.
      if ($options['relaxed']) {
        $this->logger()->notice(dt('Latest entity ID on @type is @last_id and it is over the requested new next ID @next_id, skipping.', [
          '@type' => $entity_type_id,
          '@last_id' => $last_id,
          '@next_id' => $next_id,
        ]));
      }
      else {
        throw new \InvalidArgumentException(dt('Next entity ID must be greater than the lastest ID @last_id.', ['@last_id' => $last_id]));
      }
    }

    $this->setNextId($entity_type->getBaseTable(), $next_id);
  }

  /**
   * Establishes the revsion ID for the next instance of a given entity type.
   *
   * @param string $entity_type_id
   *   The entity type machine name.
   * @param int $revision_id
   *   The revision ID of the next content instance.
   * @param array $options
   *   Additional options for the command.
   *
   * @usage entity_id_tools:next_revision_id node 100
   *   Sets 100 as the next node revision ID.
   *
   * @command entity_id_tools:next_revision_id
   * @option relaxed
   *   Do not exit with error if new ID is already assigned.
   */
  public function nextRevisionId(string $entity_type_id, int $revision_id, array $options = [
    'relaxed' => FALSE,
  ]) {
    if ($revision_id < 1) {
      // We cannot set revision IDs under 1.
      throw new \InvalidArgumentException(dt('The revision ID must be a positive integer.'));
    }

    $entity_type = $this->getEntityDefinition($entity_type_id);
    if (!$entity_type->isRevisionable()) {
      // The given entity type must be revisionable.
      throw new \InvalidArgumentException(dt('@type is not a revisionable content entity type.', ['@type' => $entity_type_id]));
    }

    /** @var \Drupal\Core\Entity\ContentEntityStorageInterface $storage */
    $storage = $this->entityTypeManager->getStorage($entity_type_id);
    $revision_id_key = $entity_type->getKey('revision');
    $last_id_result = $storage->getQuery()
      ->accessCheck(FALSE)
      ->sort($revision_id_key, 'DESC')
      ->range(0, 1)
      ->execute();
    if (!empty($last_id_result) && $revision_id <= ($last_id = array_key_first($last_id_result))) {
      // Existing content found.
      if ($options['relaxed']) {
        $this->logger()->notice(dt('Latest revision ID on @type is @last_id and it is over the requested new next ID @next_id, skipping.', [
          '@type' => $entity_type_id,
          '@last_id' => $last_id,
          '@next_id' => $revision_id,
        ]));
      }
      else {
        throw new \InvalidArgumentException(dt('Next revision ID must be greater than the lastest ID @last_id.', ['@last_id' => $last_id]));
      }
    }

    $this->setNextId($entity_type->getRevisionTable(), $revision_id);
  }

  /**
   * Gets the definition for a given entity type ID.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   *
   * @return \Drupal\Core\Entity\EntityTypeInterface
   *   The entity type definition.
   *
   * @throws \InvalidArgumentException
   *   If the given entity ID is unknown or not a content entity type.
   */
  protected function getEntityDefinition(string $entity_type_id) {
    if (!$this->entityTypeManager->hasDefinition($entity_type_id)) {
      // The given entity type must exists.
      throw new \InvalidArgumentException(dt('Entity type @type not found.', ['@type' => $entity_type_id]));
    }

    $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);
    if (!is_a($entity_type->getClass(), ContentEntityInterface::class, TRUE)) {
      // The given entity type must be a content entity type.
      throw new \InvalidArgumentException(dt('@type is not a content entity type.', ['@type' => $entity_type_id]));
    }

    return $entity_type;
  }

  /**
   * Sets the next ID on a given database table.
   *
   * @param string $table
   *   The table name.
   * @param int $next_id
   *   The next ID.
   *
   * @throws \Exception
   *   On unsupported database engines.
   */
  protected function setNextId(string $table, int $next_id) {
    if ($this->getDatabaseConnectionProvider() == 'mysql') {
      $this->database->query('ALTER TABLE {' . $table . '} AUTO_INCREMENT = ' . $next_id . ';');
    }
    else {
      throw new \Exception(dt('Unsupported database engine.'));
    }
  }

  /**
   * Gets the database connection provider.
   *
   * Provides an alternate implementation to Connection::getProvider shipped
   * with Drupal >9.2.
   *
   * @return string|null
   *   The database provider. NULL if it was unable to determine.
   */
  protected function getDatabaseConnectionProvider() {
    if (method_exists($this->database, 'getProvider')) {
      // New on Drupal >= 9.2, see https://www.drupal.org/node/3190568
      return $this->database->getProvider();
    }

    // Alternate method for Drupal <9.2.
    $parts = explode('\\', get_class($this->database));
    return count($parts) > 4 && $parts[1] == 'Core' ? $parts[4] : FALSE;
  }

}
